import React, { useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import { addAlbum } from "../actions";

const CreateAlbumForm = ({ fetchAlbum, setFetchAlbum, addAlbum }) => {
	const history = useHistory();
	const [title, setTitle] = useState("");
	const [artist, setArtist] = useState("");
	const [price, setPrice] = useState("");
	const [tracks, setTracks] = useState("");

	const handleSubmit = (e) => {
		e.preventDefault();
		// console.log("submit form is clicked");
		// console.log(title);
		// console.log(artist);
		// console.log(price);
		// console.log(tracks);

		addAlbum({
			title: title,
			artist: artist,
			price: price,
			tracks: tracks,
		});
		//console.log("success: ", success);

		// runs this code after addAlbum is completed
		setFetchAlbum(!fetchAlbum); // lifting state up to parent
		history.push("/");

		// axios
		// 	.post("http://localhost:8088/albums", {
		// 		title: title,
		// 		artist: artist,
		// 		price: price,
		// 		tracks: tracks,
		// 	})
		// 	.then(() => {
		// 		//console.log("album created!");
		// 		setFetchAlbum(!fetchAlbum); // lifting state up to parent
		// 		history.push("/");
		// 	});
	};

	return (
		<div className="container" style={{ marginTop: 10, marginBottom: 150 }}>
			<h3>Add New Album</h3>
			<form onSubmit={handleSubmit} autoComplete="off">
				<div className="form-row">
					<div className="form-group col-md-5">
						<label htmlFor="title">Title:</label>
						<input
							id="title"
							type="text"
							className="form-control"
							value={title}
							onChange={(e) => setTitle(e.target.value)}
						/>
					</div>
					<div className="form-group col-md-5">
						<label htmlFor="artist">Artist:</label>
						<input
							id="artist"
							type="text"
							className="form-control"
							value={artist}
							onChange={(e) => setArtist(e.target.value)}
						/>
					</div>
				</div>
				<div className="form-row">
					<div className="form-group col-md-5">
						<label htmlFor="price">Price:</label>
						<input
							id="price"
							type="number"
							className="form-control"
							value={price}
							onChange={(e) => setPrice(e.target.value)}
						/>
					</div>
					<div className="form-group col-md-5">
						<label htmlFor="tracks">Number of tracks:</label>
						<input
							id="tracks"
							type="number"
							className="form-control"
							value={tracks}
							onChange={(e) => setTracks(e.target.value)}
						/>
					</div>
				</div>
				<div className="form-row">
					<div className="form-group col-md-5">
						<input
							type="submit"
							value="Create Album"
							className="btn btn-outline-secondary"
						/>
					</div>
				</div>
			</form>
		</div>
	);
};

export default connect(null, { addAlbum })(CreateAlbumForm);
