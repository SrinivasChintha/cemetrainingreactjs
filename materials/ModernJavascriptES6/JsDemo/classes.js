// class Display {
//   static err(m) {
//     console.log(`[E]: ${m}`);
//   }
//   static warn(m) {
//     console.log(`[W]: ${m}`);
//   }
//   static info(m) {
//     console.log(`[I]: ${m}`);
//   }
// }

// Display.err("EEEEK!");
// Display.warn("Unexpected item in bagging area");
// Display.info("Please scan your boarding pass");

// class Person {
//   constructor(fn, ln) {
//     this.fn = fn;
//     this.ln = ln;
//   }
//   get fullName() {
//     return this.fn + " " + this.ln;
//   }
//   set fullName(name) {
//     let parts = name.toString().split(" ");
//     this.fn = parts[0] || "";
//     this.ln = parts[1] || "";
//   }
// }
// let person = new Person("John", "Smith");
// //person.fullName = "Kari Nordmann";
// console.log(person.fullName);

class Account {
  constructor(name, bal) {
    this.name = name;
    this.bal = bal;
  }
  deposit(amt) {
    this.bal += amt;
  }
  withdraw(amt) {
    this.bal -= amt;
  }
}

// let acc1 = new Account("Peter", 1000);
// console.log(`acc1 name is ${acc1.name}, bal is ${acc1.bal}`);
// acc1.withdraw(500);
// console.log(`acc1 name is ${acc1.name}, bal is ${acc1.bal}`);

class SavingsAccount extends Account {
  constructor(name, bal, rate) {
    super(name, bal);
    this.rate = rate;
  }
  withdraw(amt) {
    if (amt <= this.bal) super.withdraw(amt);
  }
  applyInterest() {
    this.deposit(this.bal * this.rate);
  }
}

// Create subclass object.
let sa = new SavingsAccount("John", 2000, 0.01);
console.log(`initial balance: ${sa.bal}`);

// Invoke inherited method.
sa.deposit(200);
console.log(`balance after deposit 200: ${sa.bal}`);

// // Invoke overriden method.
sa.withdraw(1500);
console.log(`balance after withdraw 1500: ${sa.bal}`);

// // Invoke new method.
sa.applyInterest();
console.log(`final balance: ${sa.bal}`);
