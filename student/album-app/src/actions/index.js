//for our action creators
import axios from "axios";

export const fetchAlbumsBegin = () => {
	return {
		type: "FETCH_ALBUMS_BEGIN",
	};
};

export const fetchAlbumsSuccess = (albums) => {
	return {
		type: "FETCH_ALBUMS_SUCCESS",
		payload: albums,
	};
};

export const fetchAlbumsFailure = (error) => {
	return {
		type: "FETCH_ALBUMS_FAILURE",
		payload: { message: "Error in retrieving albums... Please try again..." },
	};
};

//  to be call call by the components
export const fetchAlbums = () => {
	//returns thunk function
	return (dispatch, getState) => {
		dispatch(fetchAlbumsBegin());
		axios.get("http://localhost:8000/albums").then(
			(res) => {
				//console.log(res);
				//setAlbums(res.data);
				dispatch(fetchAlbumsSuccess(res.data));
			},
			(error) => {
				dispatch(fetchAlbumsFailure(error));
			}
		);
	};
};

export const addAlbum = (album) => {
	//returns asyn thunk function
	return (dispatch, getState) => {
		axios.post("http://localhost:8000/albums", album).then(() => {
			console.log("album created!");
			//setFetchAlbum(!fetchAlbum); // lifting state up to parent
			//history.push("/");
		});
	};
};
