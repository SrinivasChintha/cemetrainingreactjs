import React from "react";
import Album from "./Album";
import { connect } from "react-redux";

const AlbumsListing = (props) => {
	if (props.error) {
		return <div> {props.error.message} </div>;
	}
	if (props.loading) {
		return (
			<div className="d-flex justify-content-center">
				<div className="spinner-border" role="status">
					<span className="sr-only">Loading...</span>
				</div>
			</div>
		);
	}
	return (
		<div className="container">
			<div className="row">
				{props.albums.map((album) => (
					<Album
						key={album.id}
						title={album.title}
						artist={album.artist}
						tracks={album.tracks}
					/>
				))}
			</div>
		</div>
	);
};

const mapStateToProps = (state) => {
	return {
		albums: state.albums.albums,
		error: state.albums.error,
		loading: state.albums.loading,
	};
};

export default connect(mapStateToProps)(AlbumsListing);
