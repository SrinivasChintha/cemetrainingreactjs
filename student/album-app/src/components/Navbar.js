import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = () => {
	return (
		<ul className="nav">
			<li className="nav-item">
				<NavLink
					exact
					className="nav-link text-secondary"
					to="/"
					activeClassName="app"
				>
					Home
				</NavLink>
			</li>
			<li className="nav-item">
				<NavLink
					exact
					className="nav-link text-secondary"
					to="/add"
					activeClassName="app"
				>
					Add Album
				</NavLink>
			</li>
		</ul>
	);
};

export default Navbar;
