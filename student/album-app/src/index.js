import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import rootReducer from "./reducers";
import App from "./components/App";

// const albumsReducer = (state = { albums: [] }, action) => {
// 	console.log(`Recieved ${action.type} dispatch in albums reducer`);
// 	switch (action.type) {
// 		case "FETCH_ALBUMS_BEGIN":
// 			return { ...state, loading: true, error: null };
// 		case "FETCH_ALBUMS_SUCCESS":
// 			return { ...state, albums: action.payload, loading: false }; //merging payload with the state
// 		case "FETCH_ALBUMS_FAILURE":
// 			return { ...state, albums: [], loading: false, error: action.payload };
// 		default:
// 			return state;
// 	}
// };

// const rootReducer = combineReducers({
// 	albums: AlbumsReducer,
// 	errors: ErrorReducer,
// });

const store = createStore(rootReducer, applyMiddleware(thunkMiddleware)); //needs reducers
console.log("store is ", store.getState());

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById("root")
);
