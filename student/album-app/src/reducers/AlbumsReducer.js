const initialState = { albums: [] };

export default (state = initialState, action) => {
	console.log(`Recieved ${action.type} dispatch in albums reducer`);
	switch (action.type) {
		case "FETCH_ALBUMS_BEGIN":
			return { ...state, loading: true, error: null };
		case "FETCH_ALBUMS_SUCCESS":
			return { ...state, albums: action.payload, loading: false }; //merging payload with the state
		case "FETCH_ALBUMS_FAILURE":
			return { ...state, albums: [], loading: false, error: action.payload };
		default:
			return state;
	}
};
