const ErrorReducer = (state = [], action) => {
	switch (action.type) {
		case "FETCH_ALBUMS_FAILURE":
			return [...state, action.payload];
		default:
			return state;
	}
};

export default ErrorReducer;
