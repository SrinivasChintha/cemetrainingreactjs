import AlbumsReducer from "./AlbumsReducer";
import ErrorReducer from "./ErrorReducer";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
	albums: AlbumsReducer,
	errors: ErrorReducer,
});

export default rootReducer;
