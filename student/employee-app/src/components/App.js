import React, { useState, useEffect } from "react";
import Employee from "./Employee";
import Banner from "./Banner";
import axios from "axios";
import { BrowserRouter as Router, Switch, Link, Route } from "react-router-dom";
import "./App.css";

const App = () => {
	const [employees, setEmployees] = useState([]);

	//same as component did mount in class component
	useEffect(() => {
		console.log("App is mounted");
		axios.get("http://localhost:8080/employee/find/all").then((response) => {
			console.log(response);
			setEmployees(response.data);
		});
	}, []);
	return (
		<Router>
			<div>
				<nav>
					<ul>
						<li>
							<Link to="/">Home</Link>
						</li>
					</ul>
				</nav>
			</div>

			<div className="App">
				<div className="container">
					<Banner />
					<div className="row">
						{employees.map((employee) => (
							<Employee
								id={employee.id}
								name={employee.name}
								age={employee.age}
								email={employee.email}
								salary={employee.salary}
							/>
						))}
					</div>
				</div>
			</div>
		</Router>
	);
};

export default App;
