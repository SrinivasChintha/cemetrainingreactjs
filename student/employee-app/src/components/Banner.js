import React from "react";

const Banner = () => (
	<section class="jumbotron text-center">
		<div class="container">
			<h1 class="jumbotron-heading">Employee Details</h1>
			<p class="lead text-muted">
				This is app which shows employee information
			</p>
			<p>
				<a class="btn btn-primary my-2">Visit employee site</a>
			</p>
		</div>
	</section>
);
export default Banner;
