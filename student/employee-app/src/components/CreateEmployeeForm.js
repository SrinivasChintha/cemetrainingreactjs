import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import axios from "axios";

const CreateEmployeeForm = ({ fetchEmployee, setFetchEmployee }) => {
	const history = useHistory();
	const [id, setId] = useState("");
	const [name, setName] = useState("");
	const [age, setAge] = useState("");
	const [email, setEmail] = useState("");
	const [salary, setSalary] = useState("");

	const handleSubmit = (e) => {
		e.preventDefault();
		// console.log("submit form is clicked");
		// console.log(title);
		// console.log(artist);
		// console.log(price);
		// console.log(tracks);

		axios
			.post("http://localhost:8088/employee/save", {
				id: id,
				name: name,
				age: age,
				email: email,
				salary: salary,
			})
			.then(() => {
				//console.log("album created!");
				setFetchEmployee(!fetchEmployee); // lifting state up to parent
				history.push("/");
			});
	};

	return (
		<div className="container" style={{ marginTop: 10, marginBottom: 150 }}>
			<h3>Add New Album</h3>
			<form onSubmit={handleSubmit} autoComplete="off">
				<div className="form-row">
					<div className="form-group col-md-5">
						<label htmlFor="id">Employee Id:</label>
						<input
							id="id"
							type="number"
							className="form-control"
							value={id}
							onChange={(e) => setId(e.target.value)}
						/>
					</div>
					<div className="form-group col-md-5">
						<label htmlFor="name">Name:</label>
						<input
							id="name"
							type="text"
							className="form-control"
							value={name}
							onChange={(e) => setName(e.target.value)}
						/>
					</div>
				</div>
				<div className="form-row">
					<div className="form-group col-md-5">
						<label htmlFor="age">Age:</label>
						<input
							id="age"
							type="number"
							className="form-control"
							value={age}
							onChange={(e) => setAge(e.target.value)}
						/>
					</div>
					<div className="form-group col-md-5">
						<label htmlFor="email">Email:</label>
						<input
							id="email"
							type="email"
							className="form-control"
							value={email}
							onChange={(e) => setEmail(e.target.value)}
						/>
					</div>
					<div className="form-group col-md-5">
						<label htmlFor="salary">Salary:</label>
						<input
							id="salary"
							type="salary"
							className="form-control"
							value={salary}
							onChange={(e) => setSalary(e.target.value)}
						/>
					</div>
				</div>
				<div className="form-row">
					<div className="form-group col-md-5">
						<input
							type="submit"
							value="Create Employee"
							className="btn btn-outline-secondary"
						/>
					</div>
				</div>
			</form>
		</div>
	);
};

export default CreateEmployeeForm;
