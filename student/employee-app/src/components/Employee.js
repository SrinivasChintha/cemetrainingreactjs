import React, { useState } from "react";

import EmployeeDetails from "./EmployeeDetails";
import EmployeePersonalDetails from "./EmployeePersonalDetails";
import ShowHideButton from "./ShowHideButton";

const Employee = ({ id, name, email, age, salary }) => {
	const [visible, setVisibity] = useState(false);

	return (
		<div className="col-md-4">
			<div className="card mb-4 box-shadow" style={{ width: "18rem" }}>
				<div className="card-body">
					<EmployeeDetails id={id} name={name} email={email} />
					<EmployeePersonalDetails
						age={age}
						salary={salary}
						visible={visible}
					/>
					<ShowHideButton toggle={setVisibity} visible={visible} />
				</div>
			</div>
		</div>
	);
};

export default Employee;
