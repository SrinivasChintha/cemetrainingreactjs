import React from "react";

const EmployeePersonalDetails = (props) => {
	return (
		props.visible && (
			<div>
				<p>Age: {props.age}</p>
				<p>Salary: {props.salary}</p>
			</div>
		)
	);
};

export default EmployeePersonalDetails;
