import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

const headerElement = React.createElement(
	"h1",
	{ className: "name" },
	"Header"
);
const bodyElement = React.createElement(
	"p",
	{ className: "body", id: "sample-id" },
	"hello world!!"
);
const footerElement = React.createElement("span", null, "Footer");

const main = React.createElement("div", { className: "container" }, [
	headerElement,
	bodyElement,
	footerElement,
]);

ReactDOM.render(main, document.getElementById("root"));
