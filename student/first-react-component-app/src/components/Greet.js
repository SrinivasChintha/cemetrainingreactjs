import React, { useState, useEffect } from "react";

const Greet = ({ name }) => {
	//useState makes function component as statefull component
	const [height, setHeight] = useState(185);
	const [weight, setWeight] = useState(200);

	useEffect(() => {
		console.log("This is like component did mount");
	}, []);
	useEffect(() => {
		console.log("This is like component updated");
	}, [height, weight]);
	return (
		<React.Fragment>
			<h1>Hello {name} !</h1>
			<p>
				{name} height is {height} and weight {weight}
			</p>
			<button onClick={() => setHeight(height + 1)}>Grow Height</button>
			<button onClick={() => setWeight(weight + 1)}>Increase</button>
		</React.Fragment>
	);
};

export default Greet;
