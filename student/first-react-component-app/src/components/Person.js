import React, { Component } from "react";

class Person extends Component {
	constructor(props) {
		super(props);
		this.state = { height: 188, weight: 90 };
		this.increase = this.increase.bind(this);
	}
	grow = () => this.setState({ height: this.state.height + 1 }); //binding is not required if we use arrow function
	increase() {
		this.setState((state) => {
			return {
				weight: state.weight + 1,
			};
		});
	}
	render() {
		return (
			<React.Fragment>
				<p>
					Person height is {this.state.height} and weight {this.state.weight}
				</p>
				<button onClick={this.grow}>Grow</button>
				<button onClick={this.increase}>Increase</button>
			</React.Fragment>
		);
	}
	componentDidUpdate() {
		console.log("component updated");
	}
}
export default Person;
