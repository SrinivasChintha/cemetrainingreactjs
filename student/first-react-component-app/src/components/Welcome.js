import React from "react";

class WelcomeComponent extends React.Component {
	constructor(props) {
		super(props);
		this.message = props.message;
	}
	render() {
		console.log("Render method called");
		return <h3> Welcome {this.message} !</h3>;
	}
	componentDidMount() {
		console.log("component mounted");
	}

	componentWillUnmount() {
		console.log("component unmounted");
	}
}

export default WelcomeComponent;
