import React from "react";
import ReactDOM from "react-dom";
import Greet from "./components/Greet";
//import Welcome from "./components/Welcome";
import P from "./components/Person";

const main = (
	<React.Fragment>
		<Greet name="Srinivas" />
		<Greet name="Deepthi" />
	</React.Fragment>
);

ReactDOM.render(main, document.getElementById("root"));
